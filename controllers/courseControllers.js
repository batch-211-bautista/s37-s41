const Course = require("../models/Course");

// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the reqBody and the id from the header
	2. Save the new Course to the database
*/
/*
	1. Create a conditional statement that will check if the user is an admin
	2. Create a new course object using mongoose model and the information from the reqBody
	3. Save the new User to the database
*/
module.exports.addCourse = (data) => {

	// console.log(data);
	if (data.isAdmin) {
		let newCourse = new Course({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		});

		// console.log(newCourse);
		return newCourse.save().then((result, err) => {
			if (err) {
				return false;
			} else {
				return true;
			}
		});
	} else {
		return Promise.resolve(false);
	}
};

// Retrieve all courses
/*
	1. Retrieve all the courses from the database
		Model.find({})
*/

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

// Retrieve all active courses
/*
	1. Retrieve all the courses from the database with the property of "isActive" to true
*/

module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};

// Retrieving a specific course
/*
	1. Retrieve the course that matches the courseId provided from the URL
*/

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Update a course
/*
	1. Create a variable "updatedCourse" which will contain the information retrieved from the request body
	2. Find and update the courses using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body
*/

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

// Activity
module.exports.archiveCourse = (data) => {

	if (data.isAdmin) {
		return Course.findById(data.params.courseId).then((result, err) => {
			if (err) {
				return Promise.resolve(err);
			} else {
				result.isActive = data.course.isActive;
				return result.save().then((saveResult, err) => {
					if (err) {
						return Promise.resolve(err);
					} else {
						const data = {
							message: "Course archived successfully!",
							result: saveResult
						}
						return data;
					}
				});
			}
		});
	} else {
		return Promise.resolve("You are not allowed to access this feature!")
	}

};