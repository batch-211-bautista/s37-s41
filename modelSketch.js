/*
	
App: Booking System API

Scenario:
	A course booking system application where a user can enroll into a course

Type: Course Booking System (Web App)

Description:
	A course booking system application where a user can enroll to a course
	Allows an admin to do CRUD operations
	Allows users to register into our database

Features:
	- User login (User Authentication)
	- User Registration

	Customer/Authenticated Users:
	- View Courses (all active courses)
	- Enroll Course

	Admin Users:
	- Add Course
	- Update Course
	- Archive/Unarchive a course (soft delete/reactivate the course)
	- View Courses (All courses active/inactive)
	- View/Manage User Accounts

	All Users(guests, customers, admin)
	- View Active Courses
*/

// Data Model for the Booking System

// Two-way Embedding

/*

user{
	id - unique identifier for the document
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollments: [
		{
			id - document identifier
			courseId - the unique identifier
			courseName - optional
			isPaid
			dateEnrolled
		}
	]
}

course{
	id - unique for the document
	name,
	description,
	price,
	slots,
	isActive,
	createdOn,
	enrollees: [
		{
			id - document identifier
			userId,
			isPaid,
			dateEnrolled
		}
	]
}

*/

// With Referencing

/*
	user {
		id - unique identifier for the document
		firstName,
		lastName,
		email,
		password,
		mobileNumber,
		isAdmin,
	}

	course {
		id - unique for the document
		name,
		description,
		price,
		slots,
		isActive,
		createdOn,
	}

	enrollment {
		id - unique for the document
		userId - the unique identifier for the user
		courseId - the unique identifier for the course
		courseName - optional
		isPaid,
		dateEnrolled
	}
*/